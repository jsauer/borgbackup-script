# borgbackup-script

Script for automatic backup using borg to a remote host.

Keep passphrase in ~/borg-repo-pass with mode 0600

Modified from https://borgbackup.readthedocs.io/en/stable/quickstart.html#automating-backups
