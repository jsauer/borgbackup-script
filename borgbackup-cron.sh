#!/bin/bash

RETRY=6

# Sleep 0 - 20 minutes

while [ $RETRY -gt 0 ]
do
	~/borgbackup-script/borgbackup-script.sh
	RC=$?
	if [ $RC -eq 0 ]
	then
		exit 0;
	elif [ $RC -gt 1 ]
	then
		RETRY=$((RETRY - 1))
	fi

	if [ $RETRY -gt 2 ]
	then
		MAXSLEEP=600
		MINSLEEP=60
	elif [ $RETRY -lt 2 ]
	then
		MAXSLEEP=1800
		MINSLEEP=900
	fi

	sleep $(($RANDOM % $MAXSLEEP + $MINSLEEP))
done

exit 1
